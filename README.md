# Qt for Android

In this repository you will find different branches to build Qt for Android for various versions of Qt.

If you are looking for _binary packages (*.deb)_, see [at the bottom of this page](https://salsa.debian.org/bastif/qt-android#repo-with-binary-packages).

At the time of writing this, Qt versions can be built/run on these debian versions/build environments:

| Qt Version    | Stretch (9)   | Buster (10)   | Bullseye (11) | Bookworm (12) |
|:----          |:--            |:--            |:--            |:--            |
| Qt 5.11.3     |  Y            |  Y            |  Y            |               |
| Qt 5.12.11    |  Y            |  Y            |  Y            |               |
| Qt 5.15.2     |  Y            |  Y            |  Y            |               |
| Qt 6.1        |  N*           |  N*           |  Y            |               |
| Qt 6.2        |  N*           |  N*           |  Y            |               |
| Qt 6.2.2      |  N*           |  N*           |  Y            |               |
| Qt 6.2.3      |  N*           |  N*           |  Y            |               |
| Qt 6.3.1      |  N*           |  N*           |  Y (backports)|               |
| Qt 6.3.2      |  N*           |  N*           |  Y (backports)|               |
| Qt 6.4.0      |  N*           |  N*           |  Y (backports)|               |
| Qt 6.4.1      |  N*           |  N*           |  Y            |               |
| Qt 6.4.2      |  N*           |  N*           |  Y            |               |
| Qt 6.5.0      |  N*           |  N*           |  Y            |               |
| Qt 6.5.1      |  N*           |  N*           |  Y (backports)|               |
| Qt 6.5.3      |  N*           |  N*           |  Y (backports)|               |
| Qt 6.6.{0-2}  |  N**          |  N**          |  N**          | Y             |
| Qt 6.6.3      |  N**          |  N**          |  Y (backports)| Y             |
| Qt 6.7.{0-3}  |  N**          |  N**          |  Y (backports)| Y             |
| Qt 6.8.*      |  N**          |  N**          |  Y (backports)| Y             |

Y=Yes; N=No

(*) Qt 6 needs cmake (>= 3.16) to build, so with a backported cmake, it may work on Stretch and Buster.

Qt 6.5.1 add the QtPdf module which is in the QtWebengine module which requires cmake >= 3.19.

(**) Qt 6.6 needs nodejs >= 14 to build QtPdf, if QtPdf is not needed, it may build on Bullseye.

## Get the upstream tarball

You will need to get the upstream source tarball from here:
- https://download.qt.io/archive/qt for qt5.12, qt5.15, qt6
- https://download.qt.io/new_archive/qt/ for qt5.11

Take the tarball from the "single" directory (not from the submodule directory).

Place it in the parent folder of the repo.

Then go to the root of the repo and unpack it this way:

`tar xvf ../qt-everywhere-src-<version>.tar.xz --strip=1`

## Get the Android SDK

You may use (see note below) the debian packages or download them from Google.

Debian packages are:
- android-sdk-platform-tools
- android-sdk-build-tools
- android-sdk-platform-23 ⚠️

⚠️ Note that _it is not possible to use the version of android-sdk-platform-23 shipped on Debian_ because some classes are missing. See [bug report 995186 on Debian BTS](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=995186). So in the meantime you must get the sdk-platform from Google.

If you build Qt 5.15 or above you will need to take the SDK from Google because the android-sdk-platform in Debian is too old.

If you use the google archives, unpack it to a folder that suits you. For example `/opt/android-sdk`.

| Qt Version    | minimum SDK platform level (**) |
|:----          |:--            |
| Qt 5.11.3     | android-16    |
| Qt 5.12.11    | android-16    |
| Qt 5.15.2     | android-28    |
| Qt 6.0        | android-28    |
| Qt 6.1        | android-29    |
| Qt 6.2        | android-30    |
| Qt 6.2.2      | android-31    |
| Qt 6.2.3      | android-31    |
| Qt 6.3.1      | android-31    |
| Qt 6.3.2      | android-31    |
| Qt 6.4.x      | android-31    |
| Qt 6.5.x      | android-33    |
| Qt 6.6.x      | android-33    |
| Qt 6.7.x      | android-34    |
| Qt 6.8.x      | android-34    |

(**) minimum platform level is defined [at this place in Qt Code](https://github.com/qt/qtbase/blob/6.2/mkspecs/features/android/sdk.prf#L3)

## Get the Android NDK

You need specific versions of the Android NDK to build Qt for Android. Download them from [Android NDK download page](https://developer.android.com/ndk/downloads/).

| Qt Version    | recommended NDK version |
|:----          |:--    |
| Qt 5.11.3     | r10e  |
| Qt 5.12.11    | r20b  |
| Qt 5.15.2     | r20b  |
| Qt 6.0        | r22b  |
| Qt 6.1        | r22b  |
| Qt 6.2        | r22b  |
| Qt 6.3        | r22b  |
| Qt 6.4        | r23b  |
| Qt 6.5        | r25b  |
| Qt 6.6        | r25b  |
| Qt 6.7        | r26b  |
| Qt 6.8        | r26b  |

More details here:
- for [Qt5](https://doc.qt.io/qt-5/android-getting-started.html)
- for [Qt6](https://doc.qt.io/qt-6/android.html) and in the [git repo](https://github.com/qt/qtbase/tree/6.1/cmake#cross-compiling-for-android), [Supported platforms](https://doc.qt.io/qt-6/supported-platforms.html)
- the [QT wiki](https://wiki.qt.io/Android)

Unpack it to a folder that suits you. For example /opt/android-sdk/ndk

## Other dependencies

They are managed by the build system. You may check `debian/control` for details.

## Build binary package of Qt for Android

There are some instructions to build the binary package in the `debian/README.source` file of each branch. They use `dpkg-buildpackage` or `sbuild`.

### Building using a local copy of this repo

Use of this repo and building from it is similar to what is described here for Qt package.

https://salsa.debian.org/qt-kde-team/qt/qtbase/-/blob/master/debian/README.source#L217

#### Integrating original source into local repository

  Obviously, packaging process requires to have original source next to the
  debian/ directory. Since it is not allowed to ship upstream source in the
  public packaging branches, it needs to be retrieved and managed outside
  packaging branches. Basically, there are a couple of options (for other options, see linked page):

  1) Original source is extracted to the packaging branch working tree from the tarball:
```
       # (only once) Ignore everything (upstream source) but files under debian/.
       $ git config core.excludesfile debian/upstreamignore

       # Checkout a packaging branch
       $ git checkout master

       # Remove all untracked files including ignored ones. This cleans up the
       # root directory from old upstream sources (if any)
       $ git clean -xdff

       # Extract upstream source code into packaging branch working tree
       $ tar zxvf ../qtbase-opensource-src_5.4.2.orig.tar.gz --strip=1

       # Do packaging, committing, finally push...
```

## Repo with binary packages

Binary packages are available:
  - for Ubuntu, on the [Launchpad PPA of qt-android](https://launchpad.net/~bastif/+archive/ubuntu/qt-android). Only some releases are available there. For Qt6 the Debian package permits to build all packages in a single run and without build-profiles since 6.6.1.
  - in this repo for Debian where they are built for each branch. You can find the latest builds in the "aptly jobs" of the publish stage of the [CI/CD pipelines](https://salsa.debian.org/bastif/qt-android/-/pipelines). Instructions to add the repo in your sources.list is in the artifacts.

If the URLs don't work anymore it may be because it was replaced by a newer run. Look for a more recent aptly job in the [CI/CD pipelines](https://salsa.debian.org/bastif/qt-android/-/pipelines).

The binaries are built with these packages on these Debian releases. They may be used on later releases of Debian than the ones they were built on. For example, those built on stretch permit one to install the packages on Debian releases >= stretch and on Ubuntu releases >= xenial (16.04 LTS).

| Qt Version    | SDK platform  | NDK   | Debian Release | Aptly repo URL |
|:----          |:--            |:--    |:--             |:--             |
| Qt 5.11.3     | android-21    | r10e  | stretch        | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/2013814/artifacts/aptly/index.html)
| Qt 5.12.11    | android-30    | r20b  | stretch        | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/2015121/artifacts/aptly/index.html)
| Qt 5.15.2     | android-30    | r20b  | stretch        | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/2020761/artifacts/aptly/index.html)
| Qt 5.15.6     | android-30    | r20b  | stretch        | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3322569/artifacts/aptly/index.html)
| Qt 5.15.7     | android-30    | r20b  | stretch        | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3452272/artifacts/aptly/index.html)
| Qt 5.15.8     | android-31    | r20b  | buster         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/4242839/artifacts/aptly/index.html)
| Qt 5.15.9     | android-31    | r20b  | buster         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/4244091/artifacts/aptly/index.html)
| Qt 5.15.10    | android-31    | r20b  | buster         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/4284933/artifacts/aptly/index.html)
| Qt 5.15.11    | android-31    | r20b  | stretch         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/4930589/artifacts/aptly/index.html)
| Qt 5.15.12    | android-31    | r20b  | stretch         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5345673/artifacts/aptly/index.html)
| Qt 5.15.13    | android-31    | r20b  | stretch         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5441437/artifacts/aptly/index.html)
| Qt 5.15.14    | android-31    | r20b  | stretch         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5781392/artifacts/aptly/index.html)
| Qt 5.15.15    | android-31    | r20b  | stretch         | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/6336220/artifacts/aptly/index.html)
| Qt 6.1.3      | android-30    | r22b  | bullseye       | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/2006354/artifacts/aptly/index.html)
| Qt 6.2.0      | android-30    | r22b  | bullseye       | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/2053412/artifacts/aptly/index.html)
| Qt 6.2.2      | android-31    | r23b  | bullseye       | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/2280469/artifacts/aptly/index.html)
| Qt 6.2.3      | android-31    | r23b  | bullseye       | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/2434242/artifacts/aptly/index.html)
| Qt 6.2.4      | android-31    | r23c  | bullseye       | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3282628/artifacts/aptly/index.html)
| Qt 6.3.1      | android-31    | r23c  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3172350/artifacts/aptly/index.html)
| Qt 6.3.2      | android-31    | r23c  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3288864/artifacts/aptly/index.html)
| Qt 6.4.0      | android-31    | r23c  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3336222/artifacts/aptly/index.html)
| Qt 6.4.1      | android-31    | r23c  | bullseye | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3876156/artifacts/aptly/index.html)
| Qt 6.4.2      | android-31    | r23c  | bullseye | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/3879232/artifacts/aptly/index.html)
| Qt 6.5.0      | android-33    | r25b  | bullseye | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/4215884/artifacts/aptly/index.html)
| Qt 6.5.1      | android-33    | r25b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/4247878/artifacts/aptly/index.html)
| Qt 6.5.3      | android-33    | r25b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5379949/artifacts/aptly/index.html)
| Qt 6.6.0      | android-33    | r25b  | bookworm | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/4924158/artifacts/aptly/index.html)
| Qt 6.6.1      | android-33    | r25b  | bookworm | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5207139/artifacts/aptly/index.html)
| Qt 6.6.2      | android-33    | r25b  | bookworm | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5380149/artifacts/aptly/index.html)
| Qt 6.6.3      | android-33    | r25b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5540840/artifacts/aptly/index.html)
| Qt 6.7.0      | android-34    | r26b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5564082/artifacts/aptly/index.html)
| Qt 6.7.1      | android-34    | r26b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5758097/artifacts/aptly/index.html)
| Qt 6.7.2      | android-34    | r26b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/5908583/artifacts/aptly/index.html)
| Qt 6.7.3      | android-34    | r26b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/6412668/artifacts/aptly/index.html)
| Qt 6.8.0      | android-34    | r26b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/6431239/artifacts/aptly/index.html)
| Qt 6.8.1      | android-34    | r26b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/6894551/artifacts/aptly/index.html)
| Qt 6.8.2      | android-34    | r26b  | bullseye-backports | [aptly url](https://bastif.pages.debian.net/-/qt-android/-/jobs/7137618/artifacts/aptly/index.html)
